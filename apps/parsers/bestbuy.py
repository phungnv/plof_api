# -*- coding: utf-8 -*-
import random
import requests
import traceback
from lxml import html

from .common import HEADERS_LIST
from apps.core.models import Item

# Константы
MAIN_LINK = 'https://www.bestbuy.com/site/searchpage.jsp?st={}'
COOKIE = 'tfs_upg=true; UID=90400295-5745-46a7-9df9-60bbbbf440ae; CTT=94d1964b29e0c26b7eaac9fb5b31b3b8; SID=62cc2ace-382f-49a5-a55b-c74584e7e0f7; vt=3ac58e01-77a7-11e9-91d4-12f792de0b0a; AMCVS_F6301253512D2BDB0A490D45%40AdobeOrg=1; s_ecid=MCMID%7C58275127530211502564311142891475715340; s_cc=true; AAMC_bestbuy_0=REGION%7C6; aam_uuid=64845961182713152183672355330925161518; oid=1113427227; optimizelyProtocol=https; optimizelyEndUserId=oeu1557989625660r0.9376164470113613; COM_TEST_FIX=2019-05-16T06:53:46.960Z; 52245=; tfs_upg=true; _gcl_au=1.1.1594047354.1557989633; context_id=5e2c7212-77a7-11e9-ae31-0e8f55625ce2; context_session=5e2c742e-77a7-11e9-98f9-0e8f55625ce2; customerZipCode=04401|Y; pst2=463; physical_dma=552; locDestZip=04785; locStoreId=463; CTE9=T; bby_rdp=l; intl_splash=false; CRTOABE=1; bby_cbc_lb=p-browse-e; ltc=%20; bby_prc_lb=p-prc-e; bby_basket_lb=p-basket-e; bby_loc_lb=p-loc-e; lastSearchTerm=za440169us; listFacets=undefined; aamoptsegs=aam%3D12130786; bby_ctx_lb=p-ctx-e; c2=Computers%20%26%20Tablets%3A%20Tablets%3A%20All%20Tablets%3A%20pdp; lastBrowsedCategory=pcmcat209000050008; basketTimestamp=1558273712192; sc-location-v2=%7B%22meta%22%3A%7B%22CreatedAt%22%3A%222019-05-16T06%3A54%3A19.071Z%22%2C%22ModifiedAt%22%3A%222019-05-19T13%3A50%3A24.363Z%22%2C%22ExpiresAt%22%3A%222020-05-18T13%3A50%3A24.363Z%22%7D%2C%22value%22%3A%22%7B%5C%22physical%5C%22%3A%7B%5C%22zipCode%5C%22%3A%5C%2204785%5C%22%2C%5C%22source%5C%22%3A%5C%22A%5C%22%2C%5C%22captureTime%5C%22%3A%5C%222019-05-16T06%3A54%3A18.299Z%5C%22%7D%2C%5C%22store%5C%22%3A%7B%5C%22zipCode%5C%22%3A%5C%2204401%5C%22%2C%5C%22searchZipCode%5C%22%3A%5C%2204785%5C%22%2C%5C%22storeId%5C%22%3A%5C%22463%5C%22%7D%2C%5C%22destination%5C%22%3A%7B%5C%22zipCode%5C%22%3A%5C%2204785%5C%22%7D%7D%22%7D; AMCV_F6301253512D2BDB0A490D45%40AdobeOrg=-330454231%7CMCMID%7C58275127530211502564311142891475715340%7CMCAAMLH-1558878627%7C6%7CMCAAMB-1558878627%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1558279174s%7CNONE%7CMCAID%7CNONE%7CMCCIDH%7C1790647748%7CvVersion%7C3.1.2'
REFERER = 'https://www.bestbuy.com/site/lenovo-smart-tab-p10-10-1-tablet-64gb-aurora-black/6321934.p?skuId=6321934'
ORIGIN = 'https://www.bestbuy.com'


def product_processing(prod_link, model, header, brand, parent_id):
    prod_link = prod_link + '&intl=nosplash'
    page = requests.get(prod_link, headers={'User-Agent': header, 'cookie': COOKIE, 'origin': ORIGIN}).text
    dom = html.fromstring(page)
    res = {}

    if 'Choose a country.' in page:
        return None

    res['sku'] = dom.xpath('//div[@class="model product-data"]//span[@class="product-data-value body-copy"]/text()[1]')[0]
    res['title'] = dom.xpath('.//h1[@class="heading-5 v-fw-regular"]/text()')[0]
    colors = dom.xpath('.//div[@class="variation-group image-group"]//a[@data-name]/@data-name')
    if bool(colors) is True:
        res['colors'] = ', '.join(colors)
    else:
        color = dom.xpath('.//li[contains(.,"Color") and not(contains(.,"Category"))]//div[@class="row-value col-xs-6 body-copy-lg v-fw-regular"]/text()')
        res['colors'] = ', '.join(color)

    rating = dom.xpath('.//div[@class="ugc-stat"]//span[@class="c-review-average"]/text()')
    if rating:
        res['rating'] = rating[0]
    else:
        res['rating'] = 0

    res['platform'] = Item.PLATFORM_VALUE_BESTBUY
    size = dom.xpath('.//div[@class="shop-variation-wrapper"][1]//*[@class="body-copy-lg v-fw-medium variation-name"]/text()')
    if size:
        res['size'] = ', '.join(size)
    else:
        res['size'] = 'Standard size'

    picture = dom.xpath('.//img[@class="primary-image "]/@src')
    if picture:
        picture = picture[0]
        res['picture'] = picture[:picture.index(';')]
    else:
        res['picture'] = None

    try:
        res['price'] = dom.xpath('.//div[@class="priceView-hero-price priceView-customer-price"]/span[1]/text()[2]')[0]
    except:
        return None # as no price found.

    res['url'] = prod_link
    res['condition'] = 'New'
    res['brand'] = brand

    return Item.create(res, parent_id)

def parse(brand, model_num, amazon_price, parent_id):
    res = None
    try:
        old_num = model_num
        model_num = model_num.lower()
        boundary = amazon_price*0.5

        query = model_num
        query = query.split(' ')
        query = '+'.join(query)
        link = MAIN_LINK.format(query)

        header = random.choice(HEADERS_LIST)
        page = requests.get(link, headers={'User-Agent': header, 'cookie': COOKIE, 'origin': ORIGIN}).text
        page = page.lower()
        dom = html.fromstring(page)

        if "no-results-message" in page:
            return res

        best_product_link = dom.xpath('.//div[@class="list-item lv " and contains(.,"{0}")]//div[@class="sku-title"]//a/@href'.format(model_num))
        best_product_prices = dom.xpath('.//div[@class="list-item lv " and contains(.,"{0}")]//div[contains(@class, "pricing-price")]//div[contains(@class, "priceview")]//span[@aria-hidden="true" and position() = 1]/text()[2]'.format(model_num))

        i = 0
        for link in best_product_link:
            try:
                if float(best_product_prices[i]) > boundary:
                    product_link = 'https://www.bestbuy.com' + link
                    res = product_processing(product_link, old_num, header, brand, parent_id)
                    if res is not None:
                        return res
            except Exception as e:
                traceback.print_exc()
            i += 1

    except Exception as e:
        traceback.print_exc()

    return res
