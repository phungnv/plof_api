# -*- coding: utf-8 -*-
import os
import random
import re
import requests
import sys
from lxml import html

from .common import HEADERS_LIST
from apps.core.models import Item


MAIN_LINK = 'https://www.walmart.com/search/?cat_id=0&query={0}&spelling=false'


def model_processing(model):
    model_processed = re.sub('-','',model)
    model_processed = re.sub(' ', '', model_processed)
    model_processed = model_processed.lower()
    return model_processed


def product_processing(prod_link, model, header, brand, model_num, parent_id):
    link = 'https://www.walmart.com' + prod_link
    page = requests.get(link, headers={'User-Agent': header}).text
    dom = html.fromstring(page)

    meta_model = dom.xpath('.//meta[@itemprop="model"]/@content')
    if meta_model:
        meta_model = meta_model[0]
    else:
        return None

    meta_model = model_processing(meta_model)
    model_model = (''.join(dom.xpath('.//tr[contains(.,"Model")]/td[2]//text()'))).strip()
    man_part_num = (''.join(dom.xpath('.//tr[contains(.,"Manufacturer")]/td[2]//text()'))).strip()
    man_part_num_low = (''.join(dom.xpath('.//tr[contains(.,"manufacturer_part_number")]/td[2]//text()'))).strip()

    model_model = model_processing(model_model)
    man_part_num = model_processing(man_part_num)
    man_part_num_low = model_processing(man_part_num_low)
    models = [meta_model,model_model,man_part_num,man_part_num_low]

    if model not in models:
       return False
    title = dom.xpath('.//h2[@class="prod-ProductTitle no-margin font-normal heading-b"]/@content')[0]
    colors = dom.xpath('.//span[contains(@title,"Color")]/@data-variant-id')
    if colors:
        for i in range(len(colors)):
            colors[i] = colors[i].split("-")[1:][0]
        colors = ', '.join(colors)
    else:
        colors = 'Default color'
    rating = dom.xpath('.//span[@class="ReviewsHeader-ratingPrefix font-bold"]/text()')
    if rating:
        rating = rating[0]
    else:
        rating = 0

    size = dom.xpath('.//div[@class="variants-container variant-content-tiles-outer-wrapper" and (contains(.,"Capacity") or contains(.,"Size"))]//div[@class="variant-option-text"]/text()')
    if size:
        size = ', '.join(size)
    else:
        size = 'Default size'

    picture = dom.xpath('.//img[@data-tl-id="ProductPage-primary-image"]/@src')
    if picture:
        picture = picture[0]
    else:
        picture = dom.xpath('.//img[@class="hover-zoom-hero-image"]/@src')
        if picture:
            picture = picture[0]
        else:
            picture = None

    monthly = dom.xpath('.//span[@class="price-subscription-interval-unit"]')
    if monthly:
        #No understandable price for this product
        return None

    price = dom.xpath('.//div[@class="product-offer-price prod-NextDayCoreExperience hf-BotRow"]//span[@class="price-characteristic"]/@content')
    if price:
        price = price[0]
    else:
        return False

    if 'Used' in title:
        condition = 'Used'
    elif 'Refurbished ' in title:
        condition = 'Refurbished'
    else:
        condition = 'New'

    res = {
        'title': title,
        'brand': brand,
        'colors': colors,
        'sku': model_num,
        'rating': rating,
        'platform': Item.PLATFORM_VALUE_WALLMART,
        'picture': picture,
        'price': price,
        'condition': condition,
        'size': size,
        'url': link
    }

    return Item.create(res, parent_id);


def parse(brand, model_num, amazon_price, parent_id):
    try:
        header = random.choice(HEADERS_LIST)
        try:
            amazon_price = float(re.sub(',', '', amazon_price[1:]))
        except TypeError:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

            print("!!! GOT EXCEPTION during adding product %s" % (e))

        boundary = amazon_price*0.5
        model_processed = model_processing(model_num)

        query = model_num
        query = query.split(' ')
        query = '+'.join(query)
        link = MAIN_LINK.format(query) + '&grid=false'

        page = requests.get(link, headers={'User-Agent':header}).text
        if 'Sorry, no products matched' in page:
            return 0
        dom = html.fromstring(page)

        prices = dom.xpath('.//div[@class="price-main-block"]//span[@class="price-group"]/@aria-label')
        prices = [float(re.sub(',', '', prc[1:])) for prc in prices]
        prices = [pr for pr in prices if pr>boundary]
        prices = sorted(prices)
        if bool(prices) is False:
            return 0
        response = False
        for i in range(len(prices)):
            prices[i] = str(prices[i])
            first_part = prices[i].split('.')[0]
            second_part = prices[i].split('.')[1]
            if len(first_part) > 3:
                first_part = first_part[:-3] + ',' + first_part[-3:]
            prices[i] = first_part + '.' + second_part
        for price in prices:
            best_product_link = dom.xpath('.//div[@class="tile-content Grid-col u-size-8-10-l" and contains(.,"{0}")]//a[@class="product-title-link line-clamp line-clamp-2"]/@href'.format(price))[0]
            response = product_processing(best_product_link, model_processed, header, brand, model_num, parent_id)
            if response is not None:
                break
    except:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)

        print("!!! GOT EXCEPTION during adding product %s" % (e))
