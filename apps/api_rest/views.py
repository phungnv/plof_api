# -*- coding: utf-8 -*-
import os
import sys
import datetime
import json
from constance import config
from django.conf import settings
from django.core.cache import cache
from django.http import JsonResponse
from django.utils import timezone
from raven.contrib.django.raven_compat.models import client as raven_client
from rest_framework import serializers
from rest_framework.decorators import api_view, detail_route
from rest_framework.decorators import permission_classes
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import AllowAny
from rest_framework.viewsets import ViewSet

from apps.core.models import User, Brand, Color, Size, Item, Condition
from apps.core.serializers import (BrandSerializer, ColorSerializer, ConditionSerializer, SizeSerializer, ItemSerializer, ItemAltSerializer)


@api_view(['GET'])
@permission_classes([AllowAny, ])
def get_constants(request):
    res = []

    cache_key = 'constants'
    res_cache = cache.get(cache_key, None)
    if res_cache is not None:
        res = json.loads(res_cache)
    else:
        res.append({
            'name': 'BRANDS',
            'values': BrandSerializer(Brand.objects.all(), many=True).data
        })
        res.append({
            'name': 'COLORS',
            'values': ColorSerializer(Color.objects.all(), many=True).data
        })
        res.append({
            'name': 'SIZES',
            'values': SizeSerializer(Size.objects.all(), many=True).data
        })
        res.append({
            'name': 'CONDITIONS',
            'values': ConditionSerializer(Condition.objects.all(), many=True).data
        })
        res.append({
            'name': 'EXCHANGE_RATE',
            'values': config.EXCHANGE_RATE
        })
        res.append({
            'name': 'SEARCH_PAGE',
            'values': settings.REST_FRAMEWORK['PAGE_SIZE']
        })

        res_cache = json.dumps(res)
        try:
            cache.set(cache_key, res_cache, 60*5)
        except Exception as e:
            raven_client.captureException()

    return JsonResponse({'result': res})


class ItemsListView(ListCreateAPIView, RetrieveUpdateAPIView, ViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    permission_classes = (AllowAny,)
    paginator = PageNumberPagination()

    def get_queryset(self):
        q = self.request.GET.get('q', None)
        res = Item.objects.filter(parent=None)

        if q is not None:
            res = Item.objects.search(q).filter(parent=None)

        pmin = self.request.GET.get('pmin', None)
        pmax = self.request.GET.get('pmax', None)

        if pmin:
            pmin = pmin.split('$')[0]
            res = res.filter(price__gte=pmin)

        if pmax:
            pmax = pmax.split('$')[0]
            res = res.filter(price__lte=pmax)

        return res

    @detail_route(methods=['GET'])
    def alternatives(self, request, *args, **kwargs):
        obj = self.get_object()
        res = []

        if obj.cached_date is None or obj.cached_date < (timezone.now() - datetime.timedelta(hours=1)):
            from apps.parsers import bestbuy, ebay, wallmart
            obj.alternatives.all().delete()

            try:
                bestbuy.parse(obj.brand.name if obj.brand else '', obj.sku, obj.price, obj.id)
                ebay.parse(obj.brand.name if obj.brand else '', obj.sku, obj.price, obj.id)
                wallmart.parse(obj.brand.name if obj.brand else '', obj.sku, obj.price, obj.id)
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)

                print("!!! GOT EXCEPTION during adding product %s" % (e))

            obj.cached_date = timezone.now()
            obj.save()
            obj.refresh_from_db()

            ser = ItemAltSerializer(obj.alternatives.all(), many=True)
            res = ser.data
        else:
            ser = ItemAltSerializer(obj.alternatives.all(), many=True)
            res = ser.data

        return JsonResponse({'result': res})

    # def paginate_queryset(self, *args, **kwargs):
        # if self.request.GET.get('archived') or self.request.GET.get('page'):
            # return super(AlertsListView, self).paginate_queryset(*args, **kwargs)
        # else:
            # return None
